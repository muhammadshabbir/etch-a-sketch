# Etch-A-Sketch

## Description

This is a website where you draw.
https://muhammadshabbir.gitlab.io/etch-a-sketch/

## Authors and acknowledgment

Credit for images
 - Color picker and favicon icons created by Freepik - Flaticon
## License

All rights are reserved.
