function addCopyright(){
	const copyright = document.querySelector('#copyright');
	const year = new Date().getFullYear();
	copyright.textContent = `Copyright © ${year} muhammadshabbir`
};

function addGrid(gridRes = 16){
	const grid = document.querySelector('#grid');
	const length = (grid.offsetHeight / gridRes) + "px"; /*The dimensions of a pixel on the grid is the dimensions of the grid divided by the number of pixels.*/ 
	const square = document.createElement('div');
	square.style.width = length;
	square.style.height = length;

	square.classList.add('square');
	for (let i=0; i<gridRes**2; i++){
		const tempSquare = square.cloneNode();
		tempSquare.setAttribute('id', `square-${i}`)
		grid.appendChild(tempSquare);
		addPaintEvent(tempSquare);
	};
};

function removeGrid(){
	const grid = document.querySelector('#grid');
	squares = document.querySelectorAll('.square');
	squares.forEach((square) => {
		grid.removeChild(square);
	});
};

function paintClick(e){
	if (isDrawing && e.target.classList.contains('square')){
		if (rainbowColorState){
		getRandomRainbowColor();
			e.target.style.backgroundColor = `${color}`;
		}
		else{
			e.target.style.backgroundColor = `${color}`;
		}
	};
};


function paintDrag(e){
	if (isDrawing && e.target.classList.contains('square')){
		if (rainbowColorState){
			getRandomRainbowColor();
			e.target.style.backgroundColor = `${color}`;
		}
		else{
			e.target.style.backgroundColor = `${color}`;
		}
	};
};

function addPaintEvent(square){

	square.addEventListener('mousedown', paintClick);

	square.addEventListener('mouseenter', paintDrag);

};

function getRandomRainbowColor(){
	colorsArray = ['#FF0000', '#FF7F00', '#FFFF00', '#00FF00',
				   '#0000FF', '#4B0082', '#9400D3'];
	const randomNumber = Math.floor(Math.random() * 7);
	color = colorsArray[randomNumber];
};

var isDrawing = false;
var color = '#000000';
var rainbowColorState = false;
function main(){
		
	/*Adding a flag for mousedown*/
	document.addEventListener('mousedown', () => {
		isDrawing = true;
	}, {capture: true});
	
	addGrid();
	/*Adding an event listener for changing the colour*/
	const colorPicker = document.querySelector('[type=color]');
	colorPicker.addEventListener('input', () => {
		color = colorPicker.value;
	});
	/*Adding an event listener for changing the number of pixels*/
	const slider = document.querySelector('[type=range]');
	sliderValue = 16;
	slider.addEventListener('input', () => {
		removeGrid();
		sliderValue = slider.value;
		const para = document.querySelector('#para');
		para.textContent = `${sliderValue}x${sliderValue}`
		addGrid(sliderValue);
	});
	
	/*Adding button functionalities*/

	let rainbowModeState = false;
	let erasorModeState = false;

	const rainbowMode = document.querySelector('#rainbowMode');
	rainbowMode.addEventListener('click', () => {
		rainbowMode.classList.toggle('clicked');
		rainbowColorState = !rainbowColorState;
		if (!rainbowColorState){
			color = colorPicker.value;
		}

		if (erasorModeState){
			erasorModeState = !erasorModeState;
			erasorMode.classList.toggle('clicked');
		}
	});

	const erasorMode = document.querySelector('#erasorMode');
	erasorMode.addEventListener('click', () => {
		erasorModeState = !erasorModeState;
		erasorMode.classList.toggle('clicked');
		if (erasorModeState) {
			color = 'white';
		}
		else {
			color = colorPicker.value;
		};

		if (rainbowColorState){
			console.log("Test")
			rainbowMode.classList.toggle('clicked');
			rainbowColorState = !rainbowColorState;
		}
	});

	const clearMode = document.querySelector('#clearMode');
	clearMode.addEventListener('click', () => {
		removeGrid();
		addGrid(sliderValue);

		if (erasorModeState){
			erasorModeState = !erasorModeState;
			erasorMode.classList.toggle('clicked');
			color = colorPicker.value;

		}

		if (rainbowColorState){
			rainbowMode.classList.toggle('clicked');
			rainbowColorState = !rainbowColorState;
			color = colorPicker.value;
		}

	});

	/*Updating the flag for mouseup*/
	document.addEventListener('mouseup', () => {
		isDrawing = false;
	
	});
	addCopyright();
};

main();